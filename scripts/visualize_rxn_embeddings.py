from __future__ import division
from __future__ import unicode_literals

import sys
import os
from functools import reduce
import random


import multiprocessing
import logging

from torch_geometric.nn import MessagePassing
from torch_geometric.utils import add_self_loops, degree
from torch_geometric.data import Data, DataLoader, Batch

from torch.nn import Linear
from torch.nn import BatchNorm1d

from torch_geometric.nn import GCNConv
from torch_geometric.nn import GATConv
from torch_geometric.nn import ChebConv
from torch_geometric.nn import global_add_pool, global_mean_pool

from torch_geometric.utils import add_self_loops, degree


from torch_scatter import scatter_mean

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data.sampler import SubsetRandomSampler

import matplotlib.pyplot as plt

import pandas as pd
import numpy as np

import chem_to_graph

import atom_pair_net


def main():
    infile = pd.read_csv('../data/meta_cyc_atom_mappings.tsv', sep='\t')
 
    dataset, enzyme_map = chem_to_graph.file_to_dataset(infile, biochem=True)


    with open('../data/rxn_to_ec.tsv', 'w') as outF:
        outF.write('rxn\tec\n')
        for rxn in enzyme_map:
            if enzyme_map[rxn]:
                ec = enzyme_map[rxn][0]
            else:
                ec = 'NA'
            line = str(rxn) + '\t' + ec + '\n'
            outF.write(line)


    
    n_atom_features=75
    n_edge_features=6
    embed_dim=30
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    embeddings_fp = '../data/metacyc_all_embeddings_100_rxn_embed.tsv'
    with open(embeddings_fp, 'w') as outF:
        header_list = ['rxn_id', 'reactant_id', 'atom_reactivity'] + \
            ['feat_' + str(x) for x in range(embed_dim * 2)]
        
        header_str = '\t'.join(header_list) + '\n'
        outF.write(header_str)
        

    
    model = atom_pair_net.Atom_Pair_Reactivity_Net(n_atom_features, embed_dim, n_edge_features, 0, embeddings_fp=embeddings_fp).to(device)
    model.load_state_dict(torch.load('../models_metacyc_100_rxn_embed.pkl'))
    model.eval()

    
    loader = DataLoader(dataset, batch_size=16)
    atom_pair_net.test(loader, model, device)

    pd.read_csv('../data/metacyc_100_embeddings.tsv',
                sep='tsv')
    


main()
