import sys
import os
sys.path.append(os.path.abspath('/Users/jbishai/Projects/dijk-lab/utility_scripts/'))
import PHATE_vis
import pandas as pd
import re


def ec_regex(x, regex):
    res = re.search(regex, x)

    if res == None:
        return 'NA'
    else:
        return res.group(0) 

def embeddings_and_meta(df):
    dat = df.filter(regex='feat').to_numpy()
    ec_1st = df['ec_1st']
    ec_2nd = df['ec_2nd']
    return dat, ec_1st, ec_2nd

def main():
    df = pd.read_csv('../data/metacyc_100_embeddings.tsv', sep='\t')
    rxn_embeddings = df.filter(regex='feat|rxn_id').groupby('rxn_id').mean()
    reactant_embeddings = df.filter(regex='feat|rxn_id|reactant_id').groupby(['rxn_id', 'reactant_id']).mean()
    meta = pd.read_csv('../data/rxn_to_ec.tsv', sep='\t')

    meta['ec'] = meta['ec'].map(lambda x: str(x).replace('|', ''))


    meta['ec_1st'] = meta['ec'].map(lambda x: ec_regex(x, 'EC\-[0-9]*'))
    meta['ec_2nd'] = meta['ec'].map(lambda x: ec_regex(x, 'EC\-[0-9]*\.[0-9]*'))
    meta['rxn_id'] = meta['rxn'].map(lambda x: float(x))

    meta.set_index('rxn_id', inplace = True)
    rxn_embeddings = rxn_embeddings.join(meta)
    reactant_embeddings = reactant_embeddings.join(meta)

    rxn_embeddings_dat, rxn_embeddings_ec_1st, rxn_embeddings_ec_2nd = embeddings_and_meta(rxn_embeddings)
    reactant_embeddings_dat, reactant_embeddings_ec_1st, reactant_embeddings_ec_2nd = embeddings_and_meta(reactant_embeddings)

    PHATE_vis.main(rxn_embeddings_dat, rxn_embeddings_ec_1st, 'ec_1st', '100 Reaction Embddings')
    PHATE_vis.main(rxn_embeddings_dat, rxn_embeddings_ec_2nd, 'ec_2nd', '100 Reaction Embddings')

    PHATE_vis.main(reactant_embeddings_dat, reactant_embeddings_ec_1st, 'ec_1st', '100 Reactant Embddings')
    PHATE_vis.main(reactant_embeddings_dat, reactant_embeddings_ec_2nd, 'ec_2nd', '100 Reactant Embddings')

    

main()



    

    
