#!/usr/bin/env python3

from functools import reduce
import operator
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

import torch
import torch.nn as nn
from torch.utils.data.sampler import SubsetRandomSampler
from torch_geometric.data import Data, DataLoader, Batch

from chem_space import Node_Embed
from rxn_to_graphs import TripartiteData

DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

class Mol_Weight_Classifier(nn.Module):
    def __init__(self, in_channels, hidden_size, out_channels, n_hidden_layers=1):
        super(EC_Classifier, self).__init__()
        self.input_layer = nn.Sequential(
            nn.Linear(in_channels, hidden_size),
            nn.LeakyReLU()
        )
        self.hidden_layers=nn.ModuleList()
        for i in range(n_hidden_layers - 1):
            self.hidden_layers.append(
                nn.Sequential(
                    nn.Linear(hidden_size, hidden_size),
                    nn.LeakyReLU()
                )
            )
        self.hidden_layers.append(
            nn.Sequential(
                nn.Linear(hidden_size, out_channels),
                nn.Sigmoid()
            )
        )
    def forward(self, x):
        x = self.input_layer(x)
        for layer in self.hidden_layers:
            x = layer(x)
        return x


def setup_models(num_classes, cont_model_path='../models/constrastive_loss_epoch_80.pt'):
    n_atom_feats = 75
    n_edge_feats = 6
    embed_dim = 256
    null_embed = Node_Embed(n_atom_feats, embed_dim, n_edge_feats, n_layers=3)
    trained_embed = Node_Embed(n_atom_feats, embed_dim, n_edge_feats, n_layers=3)
    embed_params = torch.load(cont_model_path)
    trained_embed.load_state_dict(embed_params['model_state_dict'])

    null_classifier = EC_Classifier(embed_dim * 2, int(embed_dim * 3), num_classes, 2)
    trained_classifier = EC_Classifier(embed_dim * 2, int(embed_dim * 3), num_classes, 2)

    return null_embed, trained_embed, null_classifier, trained_classifier


def data_to_loaders(fp):
    data = torch.load(fp)

    print(data[0])

    ec_1_size = 7#data[0]['ec_1'].size()[0]
    ec_2_size = 71#data[0]['ec_2'].size()[0]

    batch_size = 2048
    validation_split = .1
    shuffle_dataset = True
    random_seed = 1
    dataset_size = len(data)
    indices = list(range(dataset_size))
    split = int(np.floor(validation_split * dataset_size))
    if shuffle_dataset:
        np.random.seed(random_seed)
        np.random.shuffle(indices)
        train_indices, val_indices = indices[split:], indices[:split]
    train_sampler = SubsetRandomSampler(train_indices)
    valid_sampler = SubsetRandomSampler(val_indices)
    train_loader = DataLoader(data, batch_size=batch_size,
                              sampler=train_sampler)
    validation_loader = DataLoader(data, batch_size=batch_size,
                                   sampler=valid_sampler)
    return train_loader, validation_loader, ec_1_size, ec_2_size


def all_to_gpu(data, device):
    #Batch(ec=[9], edge_attr_1step=[332, 6], edge_attr_p=[332, 6], edge_attr_r=[332, 6], edge_attr_rand=[414, 6], edge_index_1step=[2, 332], edge_index_p=[2, 332], edge_index_r=[2, 332], edge_index_rand=[2, 414], molecule_id_p=[9], molecule_id_r=[9], rxn_id=[9], x_1step=[161, 75], x_p=[161, 75], x_r=[161, 75], x_rand=[201, 75])

    data.x_p = data.x_p.to(device)
    data.edge_index_p = data.edge_index_p.to(device)
    data.edge_attr_p = data.edge_attr_p.to(device)

    data.x_r = data.x_r.to(device)
    data.edge_index_r = data.edge_index_r.to(device)
    data.edge_attr_r = data.edge_attr_r.to(device)

    data.x_1step = data.x_1step.to(device)
    data.edge_index_1step = data.edge_index_1step.to(device)
    data.edge_attr_1step = data.edge_attr_1step.to(device)
    data.ec_1 = data.ec_1.to(device)
    data.ec_2 = data.ec_2.to(device)

    rxn_ids = reduce(operator.add, data.rxn_id)
    rxn_ids = torch.tensor(rxn_ids)
    rxn_ids = rxn_ids.to(device)
    data.rxn_id = rxn_ids

def groupby_mean(value:torch.Tensor, labels:torch.LongTensor) -> (torch.Tensor, torch.LongTensor):
    """Group-wise average for (sparse) grouped tensors

    Args:
        value (torch.Tensor): values to average (# samples, latent dimension)
        labels (torch.LongTensor): labels for embedding parameters (# samples,)

    Returns:
        result (torch.Tensor): (# unique labels, latent dimension)
        new_labels (torch.LongTensor): (# unique labels,)

    Examples:
        >>> samples = torch.Tensor([
                             [0.15, 0.15, 0.15],    #-> group / class 1
                             [0.2, 0.2, 0.2],    #-> group / class 3
                             [0.4, 0.4, 0.4],    #-> group / class 3
                             [0.0, 0.0, 0.0]     #-> group / class 0
                      ])
        >>> labels = torch.LongTensor([1, 5, 5, 0])
        >>> result, new_labels = groupby_mean(samples, labels)

        >>> result
        tensor([[0.0000, 0.0000, 0.0000],
            [0.1500, 0.1500, 0.1500],
            [0.3000, 0.3000, 0.3000]])

        >>> new_labels
        tensor([0, 1, 5])
    """
    uniques = labels.unique().tolist()
    labels = labels.tolist()

    key_val = {key: val for key, val in zip(uniques, range(len(uniques)))}
    val_key = {val: key for key, val in zip(uniques, range(len(uniques)))}

    labels = torch.LongTensor(list(map(key_val.get, labels)))
    labels = labels.to(DEVICE)

    labels = labels.view(labels.size(0), 1).expand(-1, value.size(1))

    unique_labels, labels_count = labels.unique(dim=0, return_counts=True)
    result = torch.zeros_like(unique_labels, dtype=torch.float).scatter_add_(0, labels, value)
    result = result / labels_count.float().unsqueeze(1)
    new_labels = torch.LongTensor(list(map(val_key.get, unique_labels[:, 0].tolist())))
    return result, new_labels

def get_loss(embed_net, classifier_net, loader, optimizer, trainset=True):
    loss_fn = nn.CrossEntropyLoss()
    if trainset:
        classifier_net.train()
    else:
        classifier_net.eval()
    n = 0
    loss_all = 0
    total_positives = 0
    for data in loader:
        optimizer.zero_grad()
        all_to_gpu(data, DEVICE)


        ### debugging
        if not (max(data.edge_index_r[0]) < data.x_r.size()[0]).item():
            print('reactants are screwed')
            print('nodes')
            print(data.x_r.size())
            print('Max edge index')
            print(max(data.edge_index_r[0]))

        if not (max(data.edge_index_p[0]) < data.x_p.size()[0]).item():
            print('products are screwed')

            print('nodes')
            print(data.x_p.size())
            print('Max edge index')
            print(max(data.edge_index_p[0]))
            continue

        if not (max(data.edge_index_1step[0]) < data.x_1step.size()[0]).item():
            print('one steps are screwed')

            print('nodes')
            print(data.x_1step.size())
            print('Max edge index')
            print(max(data.edge_index_1step[0]))

        rxn_ids = data.rxn_id

        r_embed = embed_net(data.x_r, data.edge_index_r, data.edge_attr_r)
        p_embed = embed_net(data.x_p, data.edge_index_p, data.edge_attr_p)

        r_graph_embed, lab = groupby_mean(r_embed, rxn_ids)
        p_graph_embed, lab = groupby_mean(p_embed, rxn_ids)

        joined_graph = torch.cat([r_graph_embed, p_graph_embed], dim = -1)


        res = classifier_net(joined_graph)

        # size is currently hard-coded here but I can pass it eventually
        batch_size = int(data.ec_1.size()[0] / 7)
        n += batch_size

        reshaped = data.ec_1.view(batch_size, 7)


        positives = torch.argmax(reshaped, dim=-1) == torch.argmax(res, dim=-1)
        positives = torch.sum(positives)
        total_positives += positives

        ecs = torch.argmax(reshaped, dim=-1)

        loss = torch.mean(loss_fn(res, ecs))

        loss_all = loss

        if trainset:
            loss.backward()
            optimizer.step()

    return loss_all, total_positives / n


def train(embed_net, classifier_net, train_loader,
          validation_loader, optimizer):

    train_loss, train_acc = get_loss(embed_net, classifier_net, train_loader,
                          optimizer, trainset=True)

    validation_loss, validation_acc = get_loss(embed_net, classifier_net, validation_loader,
                               optimizer, trainset=False)

    return train_loss, train_acc, validation_loss, validation_acc


def detach_and_move_to_cpu(x):
    for i in range(len(x)):
        x[i] = x[i].detach().cpu()

def train_nets(train_loader, validation_loader, ec_size):
    null_embed, trained_embed, null_classifier, trained_classifier = \
        setup_models(1)

    device = DEVICE

    null_embed.to(device)
    trained_embed.to(device)

    null_classifier.to(device)
    trained_classifier.to(device)

    num_epochs = 5
    null_optimizer = torch.optim.Adam(list(null_embed.parameters()) + list(null_classifier.parameters()), lr=0.0005)
    trained_optimizer = torch.optim.Adam(list(trained_embed.parameters()) + list(trained_classifier.parameters()), lr=0.0005)

    train_dict_null = {}
    valid_dict_null = {}

    train_dict_trained = {}
    valid_dict_trained = {}

    acc_dict_null = {}
    acc_dict_trained = {}

    print('Null Model')
    for epoch in range(1, num_epochs):
        train_loss, train_acc, validation_loss, valid_acc = train(null_embed,
                                            null_classifier,
                                            train_loader,
                                            validation_loader,
                                            null_optimizer
                                            )
        train_dict_null[epoch] = train_loss.detach()
        valid_dict_null[epoch] = validation_loss.detach()
        acc_dict_null[epoch] = (train_acc, valid_acc)
        print('Epoch: %i: Null Train loss:%.4f Null Validation_loss:%.4f, Train_acc:%.4f, Validation_acc:%.4f' % (epoch, train_loss, validation_loss, train_acc, valid_acc), flush=True)

    print('Trained Model')
    for epoch in range(1, num_epochs):
        train_loss, train_acc, validation_loss, valid_acc = train(trained_embed,
                                            trained_classifier,
                                            train_loader,
                                            validation_loader,
                                            trained_optimizer
                                            )
        train_dict_trained[epoch] = train_loss.detach()
        valid_dict_trained[epoch] = validation_loss.detach()
        acc_dict_trained[epoch] = (train_acc, valid_acc)
        print('Epoch: %i: Trained Train loss:%.4f Trained Validation_loss:%.4f, Train_acc:%.4f, Validation_acc:%.4f' % (epoch, train_loss, validation_loss, train_acc, valid_acc), flush=True)


    train_vals_null = train_dict_null.values()
    valid_vals_null = valid_dict_null.values()
    train_vals_null = [x.detach().cpu() for x in train_vals_null]
    valid_vals_null = [x.detach().cpu() for x in valid_vals_null]

    train_vals_trained = train_dict_trained.values()
    valid_vals_trained = valid_dict_trained.values()
    train_vals_trained = [x.detach().cpu() for x in train_vals_trained]
    valid_vals_trained = [x.detach().cpu() for x in valid_vals_trained]


    train_acc_null, valid_acc_null = zip(*[acc_dict_null[key] for key in acc_dict_null])
    train_acc_trained, valid_acc_trained = zip(*[acc_dict_trained[key] for key in acc_dict_trained])

    print(train_acc_null)
    detach_and_move_to_cpu(train_acc_null)
    detach_and_move_to_cpu(valid_acc_null)
    detach_and_move_to_cpu(train_acc_trained)
    detach_and_move_to_cpu(valid_acc_trained)




    plt.subplot(1, 2, 1)
    plt.title('Loss Of Contrastive and Null Model')

    plt.plot(train_dict_null.keys(), train_vals_null, label = 'Null Train Loss')
    plt.plot(valid_dict_null.keys(), valid_vals_null, label = 'Null Validation Loss')

    plt.plot(train_dict_trained.keys(), train_vals_trained, label = 'Trained Train Loss')
    plt.plot(valid_dict_trained.keys(), valid_vals_trained, label = 'Trained Validation Loss')
    plt.legend()
    plt.subplot(1, 2, 2)

    plt.title('Accuracy of Contrastive and Null Model')

    plt.plot(acc_dict_null.keys(), train_acc_null, label='Null Train Accuracy')
    plt.plot(acc_dict_null.keys(), valid_acc_null, label='Null Validation Accuracy')

    plt.plot(acc_dict_trained.keys(), train_acc_trained, label=' Trained Train Accuracy')
    plt.plot(acc_dict_trained.keys(), valid_acc_trained, label='Trained Validation Accuracy')
    plt.legend()
    plt.savefig('../figures/ec_prediction_contrastive_v_null_loss_and_acc.pdf')




def main():
    random_seed = 42
    torch.manual_seed(random_seed)

    train_loader, validation_loader, ec_1_size, ec_2_size = data_to_loaders('../data/dataset_with_ecs.pt')

    train_nets(train_loader, validation_loader, ec_1_size)






main()
