#! /Users/jbishai/miniconda3/envs/gptn/bin/python

from __future__ import division
from __future__ import unicode_literals

import torch
import rdkit
from rdkit import Chem
from rdkit.Chem import AllChem
from rdkit import RDLogger

import sys
import re
import os
import numpy as np
from torch_geometric.data import Data, DataLoader, Batch
import pandas as pd
import random
from functools import reduce
import pythoncyc
from pythoncyc.PToolsFrame import PFrame
from rdkit.Chem.Descriptors import ExactMolWt

NUM_EC1 = 7
NUM_EC2 = 71


# following code was borrowed from deepchem
# https://raw.githubusercontent.com/deepchem/deepchem/master/deepchem/feat/graph_features.py


def one_of_k_encoding(x, allowable_set):
    if x not in allowable_set:
        raise Exception("input {0} not in allowable set{1}:".format(
            x, allowable_set))
    return list(map(lambda s: x == s, allowable_set))


def one_of_k_encoding_unk(x, allowable_set):
    """Maps inputs not in the allowable set to the last element."""
    if x not in allowable_set:
        x = allowable_set[-1]
    return list(map(lambda s: x == s, allowable_set))


def get_intervals(l):
    """For list of lists, gets the cumulative products of the lengths"""
    intervals = len(l) * [0]
    # Initalize with 1
    intervals[0] = 1
    for k in range(1, len(l)):
        intervals[k] = (len(l[k]) + 1) * intervals[k - 1]
    return intervals


def safe_index(l, e):
    """Gets the index of e in l, providing an index of len(l) if not found"""
    try:
        return l.index(e)
    except:
        return len(l)


possible_atom_list = [
    'C', 'N', 'O', 'S', 'F', 'P', 'Cl', 'Mg', 'Na', 'Br', 'Fe', 'Ca', 'Cu',
    'Mc', 'Pd', 'Pb', 'K', 'I', 'Al', 'Ni', 'Mn'
]
possible_numH_list = [0, 1, 2, 3, 4]
possible_valence_list = [0, 1, 2, 3, 4, 5, 6]
possible_formal_charge_list = [-3, -2, -1, 0, 1, 2, 3]
possible_hybridization_list = [
    Chem.rdchem.HybridizationType.SP, Chem.rdchem.HybridizationType.SP2,
    Chem.rdchem.HybridizationType.SP3, Chem.rdchem.HybridizationType.SP3D,
    Chem.rdchem.HybridizationType.SP3D2
]
possible_number_radical_e_list = [0, 1, 2]
possible_chirality_list = ['R', 'S']

reference_lists = [
    possible_atom_list, possible_numH_list, possible_valence_list,
    possible_formal_charge_list, possible_number_radical_e_list,
    possible_hybridization_list, possible_chirality_list
]

intervals = get_intervals(reference_lists)


def get_feature_list(atom):
    features = 6 * [0]
    features[0] = safe_index(possible_atom_list, atom.GetSymbol())
    features[1] = safe_index(possible_numH_list, atom.GetTotalNumHs())
    features[2] = safe_index(possible_valence_list, atom.GetImplicitValence())
    features[3] = safe_index(possible_formal_charge_list, atom.GetFormalCharge())
    features[4] = safe_index(possible_number_radical_e_list,
                             atom.GetNumRadicalElectrons())
    features[5] = safe_index(possible_hybridization_list, atom.GetHybridization())
    return features


def features_to_id(features, intervals):
    """Convert list of features into index using spacings provided in intervals"""
    id = 0
    for k in range(len(intervals)):
        id += features[k] * intervals[k]

    # Allow 0 index to correspond to null molecule 1
    id = id + 1
    return id


def id_to_features(id, intervals):
    features = 6 * [0]

    # Correct for null
    id -= 1

    for k in range(0, 6 - 1):
        # print(6-k-1, id)
        features[6 - k - 1] = id // intervals[6 - k - 1]
        id -= features[6 - k - 1] * intervals[6 - k - 1]
    # Correct for last one
    features[0] = id
    return features


def atom_to_id(atom):
    """Return a unique id corresponding to the atom type"""
    features = get_feature_list(atom)
    return features_to_id(features, intervals)


def atom_features(atom,
                  bool_id_feat=False,
                  explicit_H=False,
                  use_chirality=False):

    #$$$
    #print(atom.GetIdx(), atom.GetAtomMapNum())
    if bool_id_feat:
        return np.array([atom_to_id(atom)])
    else:
        results = one_of_k_encoding_unk(
          atom.GetSymbol(),
          [
              'C',
              'N',
              'O',
              'S',
              'F',
              'Si',
              'P',
              'Cl',
              'Br',
              'Mg',
              'Na',
              'Ca',
              'Fe',
              'As',
              'Al',
              'I',
              'B',
              'V',
              'K',
              'Tl',
              'Yb',
              'Sb',
              'Sn',
              'Ag',
              'Pd',
              'Co',
              'Se',
              'Ti',
              'Zn',
              'H',  # H?
              'Li',
              'Ge',
              'Cu',
              'Au',
              'Ni',
              'Cd',
              'In',
              'Mn',
              'Zr',
              'Cr',
              'Pt',
              'Hg',
              'Pb',
              'Unknown'
            ]) + one_of_k_encoding(atom.GetDegree(),
                                   [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]) + \
                                   one_of_k_encoding_unk(atom.GetImplicitValence(), [0, 1, 2, 3, 4, 5, 6]) + \
                                   [atom.GetFormalCharge(), atom.GetNumRadicalElectrons()] + \
                                   one_of_k_encoding_unk(atom.GetHybridization(), [
                                     Chem.rdchem.HybridizationType.SP, Chem.rdchem.HybridizationType.SP2,
                                     Chem.rdchem.HybridizationType.SP3, Chem.rdchem.HybridizationType.
                                     SP3D, Chem.rdchem.HybridizationType.SP3D2
                                   ]) + [atom.GetIsAromatic()]
        # In case of explicit hydrogen(QM8, QM9), avoid calling `GetTotalNumHs`
        if not explicit_H:
            results = results + one_of_k_encoding_unk(atom.GetTotalNumHs(),
                                                    [0, 1, 2, 3, 4])
        if use_chirality:
            try:
                results = results + one_of_k_encoding_unk(
                    atom.GetProp('_CIPCode'),
                    ['R', 'S']) + [atom.HasProp('_ChiralityPossible')]
            except:
                results = results + [False, False
                                     ] + [atom.HasProp('_ChiralityPossible')]

    return np.array(results)


def bond_features(bond, use_chirality=False):

    bt = bond.GetBondType()
    bond_feats = [
        bt == Chem.rdchem.BondType.SINGLE, bt == Chem.rdchem.BondType.DOUBLE,
        bt == Chem.rdchem.BondType.TRIPLE, bt == Chem.rdchem.BondType.AROMATIC,
        bond.GetIsConjugated(),
        bond.IsInRing()
    ]
    if use_chirality:
      bond_feats = bond_feats + one_of_k_encoding_unk(
          str(bond.GetStereo()),
          ["STEREONONE", "STEREOANY", "STEREOZ", "STEREOE"])
    return np.array(bond_feats)

#################
# pen added
#################
def get_bond_pair(mol):
    bonds = mol.GetBonds()
    res = [[], []]
    bond_counter = 0
    for bond in bonds:
        res[0] += [bond.GetBeginAtomIdx(), bond.GetEndAtomIdx()]
        res[1] += [bond.GetEndAtomIdx(), bond.GetBeginAtomIdx()]
        ''' Debugging code
        if bond.GetBeginAtomIdx() == 8 and bond.GetEndAtomIdx() == 9:
            print('atom is')
            print(bond.GetBeginAtom().GetSymbol())
            print(bond.GetEndAtom().GetSymbol())
            print('this is bond:')
            print(bond_counter)
            print(res)
        '''

        bond_counter += 1
    return res

#def mol2vec(mol, r_atoms, rxn_id):
def mol2vec(mol, rxn_id, molecule_id, ec, ec_1, ec_2):
    mol.UpdatePropertyCache()
    atoms = mol.GetAtoms()
    bonds = mol.GetBonds()
    node_f = [atom_features(atom) for atom in atoms]
    atom_map_no = [atom.GetAtomMapNum() for atom in atoms]
    atom_idx_no = [atom.GetIdx() for atom in atoms]

    edge_index = get_bond_pair(mol)
    #y=torch.tensor([1 if (atom.GetIdx() in r_atoms) else 0 for atom in atoms])

    '''
    Theres something fucky about this
    edge_attr = [bond_features(bond, use_chirality=False) for bond in bonds]
    for bond in bonds:
        edge_attr.append(bond_features(bond))
    '''
    edge_attr = []
    for bond in bonds:
        edge_attr.append(bond_features(bond, use_chirality=False))
        edge_attr.append(bond_features(bond))


    data = Data(x=torch.tensor(node_f, dtype=torch.float),
                edge_index=torch.tensor(edge_index, dtype=torch.long),
                edge_attr=torch.tensor(edge_attr,dtype=torch.float))
                #y=1
                #)
    data.__setitem__('rxn_id', rxn_id)
    data.__setitem__('molecule_id', [molecule_id for x in range(len(atoms))])
    data.__setitem__('ec', [ec for x in range(len(atoms))])
    data.__setitem__('atom_map_no', atom_map_no)
    data.__setitem__('ec_1', ec_1)
    data.__setitem__('ec_2', ec_2)

    return data


def mol_with_atom_index(mol, reaction_no, react_or_prod_str):
    for atom in mol.GetAtoms():
        atom.SetAtomMapNum(atom.GetIdx())
    fig = Draw.MolToMPL(mol)

    fig.savefig('../figures/' + reaction_no + '/' + react_or_prod_str + '.jpg', bbox_inches='tight')
    return mol

def atom_map_dict(mol):
    # returns a dictionary of format {atom in molecule:unique atom index in reaction}
    ind_to_map = {}
    map_to_ind = {}
    for atom in mol.GetAtoms():
        map_num = atom.GetAtomMapNum()
        if map_num:
            ind_to_map[atom.GetIdx()] = map_num-1
            map_to_ind[map_num-1] = atom.GetIdx()
    return ind_to_map, map_to_ind


def find_mappings_of_bonded(bonds, ind_to_map, target_idx):
    bonded = set()
    for bond in bonds:
        start_idx = bond.GetBeginAtomIdx()
        end_idx = bond.GetEndAtomIdx()
        if start_idx == target_idx and end_idx in ind_to_map:
            bonded.add(ind_to_map[end_idx])
        elif end_idx == target_idx and start_idx in ind_to_map:
            bonded.add(ind_to_map[start_idx])
    return(bonded)


POSSIBLE_BONDS = {(True, False, False, False, False, False):1, (False, False, True, False, False, False):2,
                  (False, True, False, False, False, False):3, (True, False, False, False, False, True):4,
                  (False, True, False, False, False, True):5}

INV_POSSIBLE_BONDS = {v: k for k, v in POSSIBLE_BONDS.items()}


class ReactingAtom():
    def get_bonded_atoms(self, bonds, target_idx):

      bonded = set()
      for bond in bonds:
          bond_key = tuple(bond_features(bond).tolist())
          bond_encoding = POSSIBLE_BONDS[bond_key]


          start_idx = bond.GetBeginAtomIdx()
          end_idx = bond.GetEndAtomIdx()
          if start_idx == target_idx:
              bonded.add((end_idx, bond_encoding))
          elif end_idx == target_idx:
              bonded.add((start_idx, bond_encoding))

      return bonded
    def __init__(self, graph_ind, atom):
        self.graph_index = graph_ind
        self.index_in_mol = atom.GetIdx()
        self.atom_map = atom.GetAtomMapNum()
        self.bonds_in_molecule = self.get_bonded_atoms(atom.GetBonds(), self.index_in_mol)
        self.bonds_in_graph=[]
    def is_atom_map_match(atom_map):
        return atom_map == self.atom_map
    def is_index_match(self,idx):
        return idx == self.index_in_mol
    def convert_within_mol_bonds_to_graph_bonds(self, molecule):
        new_bonds = []
        for bond in self.bonds_in_molecule:
            new_bonds += [(atom.graph_index, bond[1]) for atom in molecule if atom.is_index_match(bond[0])]
        self.bonds_in_graph = set(new_bonds)



def symmetric_update_reaction_matrix(rxn_matrix, idx1, idx2, bond_encoding):
    #bond_encoding=1
    rxn_matrix[idx1][idx2] = bond_encoding
    rxn_matrix[idx2][idx1] = bond_encoding
    return rxn_matrix



def find_reacting_pair(rxn, reverse=True):
    rxn.Initialize()
    rxn.RemoveUnmappedReactantTemplates()

    if reverse:
        reactants = rxn.GetProducts()
        products = rxn.GetReactants()
    else:
        reactants = rxn.GetReactants()
        products = rxn.GetProducts()

    #reacting_atoms = rxn.GetReactingAtoms()
    react_ind_to_maps = []

    all_reactant_atoms = []
    graph_ind = 0
    map_idx_to_graph_idx = {}
    for reactant in reactants:
        molecule = []
        for atom in reactant.GetAtoms():
            r_atom = ReactingAtom(graph_ind, atom)
            molecule.append(r_atom)
            map_idx_to_graph_idx[atom.GetAtomMapNum()] = graph_ind
            #This might break some things
            atom.SetAtomMapNum(graph_ind)
            all_reactant_atoms.append(r_atom)
            graph_ind += 1
        for atom in molecule:
            atom.convert_within_mol_bonds_to_graph_bonds(molecule)


    all_product_atoms = {}
    for product in products:
        molecule = []
        for atom in product.GetAtoms():
            if atom.GetAtomMapNum() in map_idx_to_graph_idx:
                p_atom = ReactingAtom(map_idx_to_graph_idx[atom.GetAtomMapNum()], atom)
                #this might break some things
                graph_idx = map_idx_to_graph_idx[atom.GetAtomMapNum()]
                atom.SetAtomMapNum(graph_idx)
            molecule.append(p_atom)
        for atom in molecule:
            atom.convert_within_mol_bonds_to_graph_bonds(molecule)
            all_product_atoms[atom.graph_index] = atom

    rxn_matrix = np.zeros([len(all_reactant_atoms)] * 2, dtype = int)
    for atom in all_reactant_atoms:
        graph_idx = atom.graph_index
        if graph_idx in all_product_atoms:
            r_bonded = atom.bonds_in_graph
            p_bonded = all_product_atoms[graph_idx].bonds_in_graph
            #reacting_atoms = r_bonded.symmetric_difference(p_bonded)
            reacting_atoms = p_bonded - r_bonded
            for r_atom in reacting_atoms:
                symmetric_update_reaction_matrix(rxn_matrix, graph_idx, r_atom[0], r_atom[1])
        else:
            # atom lost from product

            reacting_atoms = atom.bonds_in_graph
            for r_atom in reacting_atoms:
                symmetric_update_reaction_matrix(rxn_matrix, graph_idx, r_atom[0], 6)
    return(rxn_matrix)

def combine_graphs(g1, g2):
  #increment every node in the second adjacency list by the number of nodes in the first adjacency list
    index_increment = g1.x.size()[0]
    combined_edge_index = torch.cat((g1.edge_index, g2.edge_index + index_increment), dim = 1)
    combined_edge_attr = torch.cat((g1.edge_attr, g2.edge_attr), dim = 0)
    combined_x = torch.cat((g1.x, g2.x), dim = 0)
    #combined_y = torch.cat((g1.y, g2.y), dim = 0)


    molecule_ids = g1.molecule_id +  g2.molecule_id
    ecs = g1.ec + g2.ec

    combined = Data(x=combined_x,
                    edge_index=combined_edge_index,
                    edge_attr=combined_edge_attr)
    combined.__setitem__('rxn_id', g1.rxn_id)
    combined.__setitem__('molecule_id', molecule_ids)
    combined.__setitem__('ec', ecs)
    combined.__setitem__('ec_1', g1.ec_1)
    combined.__setitem__('ec_2', g1.ec_2)


    return(combined)



def shuffle_tensor(tensor, swap_dict):
    shuffled = tensor.clone()
    working_dict = swap_dict.copy()
    while len(working_dict.keys()) > 0:

        idx1 = list(working_dict.keys())[0]
        idx2 = working_dict[idx1]
        shuffled[idx1] = tensor[idx2]
        shuffled[idx2] = tensor[idx1]
        working_dict.pop(idx1, None)
        working_dict.pop(idx2, None)
    return(shuffled)








def check_equivalency(r, p):
    print(torch.sum(torch.sub(r.edge_attr, p.edge_attr)))


def remove_edge_at_index(adj_list, idx):
    out_adj = torch.cat([adj_list.T[:idx], adj_list.T[idx+1:]])
    out_adj = out_adj.T
    return out_adj

def remove_edge_feat_at_index(edge_feat, idx):
    return torch.cat([edge_feat[:idx], edge_feat[idx+1:]])

def react_one_step(r, p, rxn_matrix):
    #bond_changes = (r.edge_attr.eq(p.edge_attr).sum(dim = 1) != 6).nonzero(as_tuple=True)[0]
    #selected_bond_index = bond_changes[torch.randint(len(bond_changes), (1,))]

    #print(r.x[9])


    nz = np.nonzero(rxn_matrix)

    choice_idx = random.choice(range(len(nz[0])))
    selected_bond = torch.tensor([nz[0][choice_idx], nz[1][choice_idx]])
    selected_bond_i = torch.zeros(2)
    selected_bond_i[0] = selected_bond[1]
    selected_bond_i[1] = selected_bond[0]

    #broadcast selected bond over edge indices, find position of edge where both
    #values match return that index
    selected_bond_index = r.edge_index.T.eq(selected_bond).sum(dim=1).eq(2).nonzero(as_tuple=True)[0]
    selected_bond_index_sym = r.edge_index.T.eq(selected_bond_i).sum(dim=1).eq(2).nonzero(as_tuple=True)[0]


    change_to_enact = rxn_matrix[selected_bond[0]][selected_bond[1]]

    #print('change to enact: %i' % change_to_enact)
    reacted = r.clone()


    #No bond in edge list in reactant: New bond formation
    if selected_bond_index.size() == 0:
        bond_to_add = torch.tensor(INV_POSSIBLE_BONDS[change_to_enact])
        new_adj = reacted.edge_index.T
        new_adj = torch.cat([new_adj, selected_bond])
        new_adj = torch.cat([new_adj, selected_bond_i])
        new_adj = new_adj.T
        new_feat = reacted.edge_attr
        new_feat = torch.cat([new_feat, bond_to_add])
        new_feat = torch.cat([new_feat, bond_to_add])

        reacted.edge_index = new_adj
        reacted.edge_attr = new_feat
        #print('new bonds')
    #change is loss of a bond
    if change_to_enact == 6:
        if selected_bond_index.item() > selected_bond_index_sym.item():
            new_adj = remove_edge_at_index(reacted.edge_index, selected_bond_index)
            new_adj = remove_edge_at_index(new_adj, selected_bond_index_sym)
            new_feat = remove_edge_feat_at_index(reacted.edge_attr, selected_bond_index)
            new_feat = remove_edge_feat_at_index(new_feat, selected_bond_index_sym)
        else:
            new_adj = remove_edge_at_index(reacted.edge_index, selected_bond_index_sym)
            new_adj = remove_edge_at_index(new_adj, selected_bond_index)
            new_feat = remove_edge_feat_at_index(reacted.edge_attr, selected_bond_index_sym)
            new_feat = remove_edge_feat_at_index(new_feat, selected_bond_index)
        reacted.edge_attr = new_feat
        reacted.edge_index = new_adj
        #print('broken bonds')

    # bond transmutation
    if change_to_enact != 6:
        bond_to_add = torch.tensor(INV_POSSIBLE_BONDS[change_to_enact]).float()
        reacted.edge_attr[selected_bond_index] = bond_to_add
        reacted.edge_attr[selected_bond_index_sym] = bond_to_add
        #print ('changed bond')
    return reacted


class TripartiteData(Data):
    def __init__(self, x_r, edge_index_r, edge_attr_r,
                 x_p, edge_index_p, edge_attr_p,
                 x_1step, edge_index_1step, edge_attr_1step,
                 rxn_id, molecule_id_r, molecule_id_p, ec, ec_1, ec_2,
                 mol_wt_reactants, mol_wt_products):
        super(TripartiteData, self).__init__()
        self.x_r = x_r
        self.edge_index_r = edge_index_r
        self.edge_attr_r = edge_attr_r
        self.x_p = x_p
        self.edge_index_p = edge_index_p
        self.edge_attr_p = edge_attr_p
        self.x_1step = x_1step
        self.edge_index_1step = edge_index_1step
        self.edge_attr_1step = edge_attr_1step
        self.rxn_id = rxn_id
        self.molecule_id_r = molecule_id_r
        self.molecule_id_p = molecule_id_p
        self.ec = ec
        self.ec_1 = ec_1
        self.ec_2 = ec_2
        self.mol_wt_reactants = mol_wt_reactants
        self.mol_wt_products = mol_wt_products
    def __inc__(self, key, value):
        if key == 'edge_index_r':
            return self.x_r.size(0)
        if key == 'edge_index_p':
            return self.x_p.size(0)
        if key == 'edge_index_1step':
            return self.x_1step.size(0)
        if key == 'edge_index_rand':
            return self.x_rand.size(0)
        else:
            return super().__inc__(key, value)


def forge_rxn(combined_r, combined_p, rxn_matrix, mol_wt_reactants, mol_wt_products):
    reacted = react_one_step(combined_r, combined_p, rxn_matrix)
    rxn_id = [reacted.rxn_id for x in range(reacted.x.size()[0])]
    rxn = TripartiteData(x_r=combined_r.x,
                         edge_index_r=combined_r.edge_index,
                         edge_attr_r=combined_r.edge_attr,
                         x_p=combined_r.x,
                         edge_index_p=combined_p.edge_index,
                         edge_attr_p=combined_p.edge_attr,
                         x_1step=reacted.x,
                         edge_index_1step=reacted.edge_index,
                         edge_attr_1step=reacted.edge_attr,
                         rxn_id=rxn_id,
                         molecule_id_r=combined_r.molecule_id,
                         molecule_id_p=combined_p.molecule_id,
                         ec=combined_r.ec,
                         ec_1=combined_r.ec_1,
                         ec_2=combined_r.ec_2,
                         mol_wt_reactants=mol_wt_reactants,
                         mol_wt_products=mol_wt_products)
    return rxn



def encode_reactants(rxn, rxn_id, solvent_dict={}, rxn_to_solvent_dict={}, ec='', ec_1=torch.zeros(1), ec_2=torch.zeros(1), reverse=True):
    data = []
    rxn.Initialize()
    agents = rxn.GetAgents()

    rxn.RemoveUnmappedReactantTemplates()

    rxn_matrix = find_reacting_pair(rxn)



    #reactants = rxn.GetReactants()
    #products = rxn.GetProducts()


    # Metacyc encodes their reactions backwards for some opaque reason
    # https://biocyc.org/PGDBConceptsGuide.shtml#TAG:__tex2page_sec_4.6

    if reverse:
        reactants = rxn.GetProducts()
        products = rxn.GetReactants()
    else:
        reactants = rxn.GetReactants()
        products = rxn.GetProducts()



    for agent in agents:
        agent_smiles =  Chem.MolToSmiles(agent)
        if agent_smiles not in solvent_dict:
          solvent_dict[agent_smiles] = len(solvent_dict.keys())
        rxn_to_solvent_dict[rxn_id].append(solvent_dict[agent_smiles])

    reactant_graphs = []
    molecule_id = 0
    mol_wt_reactants = 0
    mol_wt_products = 0
    for reactant in reactants:

        reactant.UpdatePropertyCache(strict=False)
        reactant = Chem.AddHs(reactant, addCoords=True)
        atoms = reactant.GetAtoms()
        mol_wt_reactants += ExactMolWt(reactant)
        reactant_graph = mol2vec(reactant, rxn_id, molecule_id, ec, ec_1, ec_2)
        molecule_id += 1

        reactant_graphs.append(reactant_graph)

    product_graphs = []
    for product in products:
        product.UpdatePropertyCache(strict=False)
        product = Chem.AddHs(product, addCoords=True)
        atoms = product.GetAtoms()
        mol_wt_products += ExactMolWt(product)
        product_graph = mol2vec(product, rxn_id, molecule_id, ec, ec_1, ec_2)
        molecule_id += 1
        product_graphs.append(product_graph)

    combined_r = reduce((lambda x,y: combine_graphs(x,y)), reactant_graphs)
    combined_p = reduce((lambda x,y: combine_graphs(x,y)), product_graphs)


    #something funny is going on here sometimes with typing of mol_wts I think
    forged = forge_rxn(combined_r, combined_p, rxn_matrix, mol_wt_reactants, mol_wt_products)

    return([forged])


def generate_solvent_tensor(n_solvents, rxn_to_solvent_dict, rxn_id):
    solvents = rxn_to_solvent_dict[rxn_id]
    solvent_list = [1 if x in solvents else 0 for x in range(n_solvents)]
    return torch.tensor(solvent_list)


def split_ec(ec):
    regex = re.search('(?<=EC\-)[^\|]*', ec)
    full_ec = regex.group(0)
    ec_split = full_ec.split('.')
    ec_1 = ec_split[0]
    ec_2 = ec_split[0] + '.' + ec_split[1]
    return (full_ec, ec_1, ec_2)


def get_ec_from_rxn(rxn_name, rxn_no, ec_dict, ec_onehot, ec_counters):
    try:
        meta = pythoncyc.select_organism('meta')
        pf = PFrame(rxn_name, meta, getFrameData=True)
        ec_str = pf.ec_number
        ec, ec_1, ec_2 = split_ec(ec_str[0])
        if ec_1 not in ec_onehot:
            ec_onehot[ec_1] = ec_counters[0]
            ec_counters[0] += 1
        if ec_2 not in ec_onehot:
            ec_onehot[ec_2] = ec_counters[1]
            ec_counters[1] += 1
        ec_dict[rxn_no] = [ec, ec_onehot[ec_1], ec_onehot[ec_2]]
        return True
    except:
        return False


def file_to_rxns(infile, fp, ec_fps, force_encode=False):

    if (os.path.exists(fp) and not force_encode):
        return torch.load(fp)


    RDLogger.DisableLog('rdApp.*')
    rxns = []
    reaction_no = 0

    ec_dict = {}
    ec_onehot = {}
    ec_counters = [0,0]
    rxn_counter = 0
    for rxn, rxn_name in zip(infile['ReactionSmiles'], infile['rxn']):

        reaction_no += 1
        rxn=rxn.split(' ')[0]


        if reaction_no not in [10]:

            try:
                get_ec_from_rxn(rxn_name, reaction_no, ec_dict, ec_onehot, ec_counters)
                ec_1_vec = [0] * NUM_EC1
                ec_2_vec = [0] * NUM_EC2
                ec = ec_dict[reaction_no][0]



                ec_1 = ec_1_vec[ec_dict[reaction_no][1]] = 1
                ec_2 = ec_2_vec[ec_dict[reaction_no][2]] = 1
                ec_1 = torch.tensor(ec_1_vec)
                ec_2 = torch.tensor(ec_2_vec)



                '''
                print('for rxn %i' % (reaction_no))
                print(ec)
                print(ec_1)
                print(ec_2)
                '''
                r = AllChem.ReactionFromSmarts(rxn)
                rxns += encode_reactants(r, reaction_no, ec=ec, ec_1=ec_1, ec_2=ec_2)

                rxn_counter += 1
            except:
                print(reaction_no)
                print(sys.exc_info())

    for i in range(len(rxns)):
        cur_mol = rxns[i]
        rand_mol = random.choice([j for h,j in enumerate(rxns) if j != i])
        cur_mol.__setitem__('x_rand', rand_mol.x_r)
        cur_mol.__setitem__('edge_attr_rand', rand_mol.edge_attr_r)
        cur_mol.__setitem__('edge_index_rand', rand_mol.edge_index_r)

    torch.save(rxns, fp)
    print('%i Reactions Successfully Encoded' % rxn_counter)
    return rxns




if __name__ == '__main__':
    #infile = pd.read_csv('../test_data/meta_cyc_atom_mappings_5.tsv', sep='\t')
    infile = pd.read_csv('../data/meta_cyc_atom_mappings.tsv', sep='\t')
    rxns = file_to_rxns(infile, '../data/dataset_with_ecs_and_mol_wt.pt', [], force_encode=True)
