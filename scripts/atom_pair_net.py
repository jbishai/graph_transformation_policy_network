from __future__ import division
from __future__ import unicode_literals

import sys
import os
from functools import reduce
import random


import multiprocessing
import logging

from torch_geometric.nn import MessagePassing
from torch_geometric.utils import add_self_loops, degree
from torch_geometric.data import Data, DataLoader, Batch

from torch.nn import Linear
from torch.nn import BatchNorm1d

from torch_geometric.nn import GCNConv
from torch_geometric.nn import GATConv
from torch_geometric.nn import ChebConv
from torch_geometric.nn import global_add_pool, global_mean_pool

from torch_geometric.utils import add_self_loops, degree


from torch_scatter import scatter_mean

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data.sampler import SubsetRandomSampler

import matplotlib.pyplot as plt

import pandas as pd
import numpy as np

import chem_to_graph


# This is part of the node embedding network as implemented in Graph
# Transofmation Policy Network for Chemical Reaction Prediction by Do et al

visualization = {}

def hook_fn(m, i, o):
  visualization[m] = 0

class Highway(nn.Module):
  def __init__(self, size, num_layers, f):
    super(Highway, self).__init__()
    self.num_layers = num_layers
    self.nonlinear = nn.ModuleList([nn.Linear(size, size) for _ in range(num_layers)])
    self.linear = nn.ModuleList([nn.Linear(size, size) for _ in range(num_layers)])
    self.gate = nn.ModuleList([nn.Linear(size, size) for _ in range(num_layers)])
    self.f = f
  def forward(self, x):
    """
    :param x: tensor with shape of [batch_size, size]
    :return: tensor with shape of [batch_size, size]
    applies σ(x) ⨀ (f(G(x))) + (1 - σ(x)) ⨀ (Q(x)) transformation | G and Q is affine transformation,
    f is non-linear transformation, σ(x) is affine transformation with sigmoid non-linearition
    and ⨀ is element-wise multiplication
    """
    for layer in range(self.num_layers):
      gate = F.sigmoid(self.gate[layer](x))
      nonlinear = self.f(self.nonlinear[layer](x))
      linear = self.linear[layer](x)
      x = gate * nonlinear + (1 - gate) * linear
      return x

# My implementation of the GNN from GTPN
# from Appendix A1 in Do et. al
class GNN(MessagePassing):
    def __init__(self, in_channels, n_edge_feats, out_channels):
        super(GNN, self).__init__(aggr='mean')  # "Add" aggregation (Step 5).
        self.lin = torch.nn.Linear(in_channels, out_channels)
        self.mlp = nn.Sequential(
          nn.Linear(in_channels * 2 + n_edge_feats, out_channels),
          nn.ReLU()
        )
        self.highway = Highway(size=in_channels + out_channels, num_layers=1, f=nn.Sigmoid())
    def forward(self, x, edge_index, edge_attr):
      # x has shape [N, in_channels]
      # edge_index has shape [2, E]
      # Linearly transform node feature matrix.
      x = self.lin(x)
      # Start propagating messages.

      return self.propagate(edge_index, x=x, edge_attr=edge_attr)#, norm=norm)

    def message(self, x_i, x_j, edge_attr):
      # Messages are the concatenation of neighboring nodes with
      # edge features then passed through a nonlinear function
      # in the event that an individual atom must be embedded (has no edges)
      # a spoofed tensor of  edge features is generated for that atom (all 0s) 
      if(edge_attr.size()[0] == 0):
        edge_attr = torch.zeros([0,6])
      tmp = torch.cat([x_i, x_j, edge_attr], dim = 1)
      return(self.mlp(tmp))

    def update(self, aggr_out, x):
      # update of node state takes node state and message
      # which together are passed through a highway network
      tmp = torch.cat([x, aggr_out], dim=1)
      return(self.highway(tmp))

# 3 message passing steps with resizing in between
class Node_Embed(torch.nn.Module):
  def __init__(self, in_channels, embed_dim, n_edge_feats):
    super(Node_Embed, self).__init__()
    self.embed = nn.Sequential(nn.Linear(in_channels, embed_dim),
                      nn.ReLU())
    self.embed2 = nn.Sequential(nn.Linear(embed_dim * 2, embed_dim),
                      nn.ReLU())

    self.embed3 = nn.Sequential(nn.Linear(embed_dim * 2, embed_dim),
                      nn.ReLU())
    
 
    self.gnn1 = GNN(embed_dim, n_edge_feats, embed_dim)
    self.gnn2 = GNN(embed_dim, n_edge_feats, embed_dim)
    self.gnn3 = GNN(embed_dim, n_edge_feats, embed_dim)


  def forward(self, x, edge_index, edge_attr, rxn_id): 
    x = self.embed(x)
    x = self.gnn1(x, edge_index, edge_attr)
    x = self.embed2(x)
    x = self.gnn2(x, edge_index, edge_attr)
    x = self.embed3(x)
    x = self.gnn3(x, edge_index, edge_attr)
    
    return x


def emit_pair(energy_mat):
  n_nodes = energy_mat.shape[0]
  energy_flat = torch.flatten(energy_mat, 0, 1)

  #probs = F.softmax(energy_flat, dim = 0)

  return(energy_flat)


def write_embeddings_to_file(embeddings, data, fp):
  with open(fp, 'a') as outF:
    rxn_ids = []
    reactant_ids = []
    ecs = []
    reactive_atoms = []
    for rxn_id, reactant_id, ec, reactive_atom in zip(data.rxn_id, data.reactant_id, data.ec, data.reactive_atoms):
      rxn_list = [rxn_id.item() for x in reactant_id]
      rxn_ids += rxn_list
      reactant_ids += reactant_id
      ecs += ec
      reactive_atoms += reactive_atom
      

    rxn_tensor = torch.tensor(rxn_ids)
    reactant_tensor = torch.tensor(reactant_ids)
    reactivity_tensor = torch.tensor(reactive_atoms)
    
    metadata = torch.stack((rxn_tensor.float(), reactant_tensor.float(), reactivity_tensor.float()), -1)
    merge = torch.cat((metadata, embeddings), 1)
    np.savetxt(outF, merge.detach().numpy(), delimiter = '\t')



def get_combo_indices(n_nodes):
  upper_left_triangle_inclusive =  torch.combinations(torch.tensor([x for x in range(n_nodes)]), r=2, with_replacement = True)
  bottom_right_triangle = torch.combinations(torch.tensor([x for x in range(n_nodes - 1, -1, -1)]), r=2, with_replacement = False)
  combos = torch.cat([upper_left_triangle_inclusive, bottom_right_triangle])
  return(combos)
    
class Atom_Pair_Reactivity_Net(torch.nn.Module):
  def __init__(self, in_channels, embed_dim, n_edge_feats, n_solvents, embeddings_fp = ''):
    super(Atom_Pair_Reactivity_Net, self).__init__()
    self.embeddings_fp = embeddings_fp
    self.embed = Node_Embed(in_channels, embed_dim, n_edge_feats)
    self.embed.register_forward_hook(hook_fn)

    self.gat = GATConv(embed_dim * 2, embed_dim)
    hidden_size = int(embed_dim *4/3.)
    self.energy = nn.Sequential(nn.Linear(embed_dim * 2 + n_solvents + embed_dim * 2, hidden_size),
                                nn.Linear(hidden_size, hidden_size),
                                nn.Linear(hidden_size, hidden_size),
                                nn.Linear(hidden_size, 2),
                                nn.Sigmoid())
    self.n_solvents = n_solvents

  
    

  def forward(self, data):
    x, edge_index, x_p, edge_index_p, edge_attr = data.x, data.edge_index, data.x_p, data.edge_index_p, data.edge_attr
    #generate node embeddings
    x = self.embed(x, edge_index, edge_attr, data.rxn_id)
    if len(self.embeddings_fp) > 0:
      write_embeddings_to_file(x, data, self.embeddings_fp)
    #print(sum([x.shape[0] for x in data.rxn_mat]))
  
    offset = 0
    results = None
    #generate the energy matrix which per pair of atoms in all molecules in a
    #reaction (embedded in previous step), calculates a normalized energy
    #from 0 to 1 (tanh)
    #outputs an energy which then is converted to a probability with softmax
    #this energy goes in an energy matrix per reaction

    #I think this is where the gradient breaks as any individual
    #node embedding is involved in multiple energy calculations, so the 
    #autograd graph probably isn't happy about that at the end of the day
    
    solvent_slice = 0
    solvent_list = data.solvents.tolist()
    all_rxns = []
    for rxn_mat in data.rxn_mat:


      n_nodes = rxn_mat.shape[0]
      n_pairs = n_nodes*(n_nodes-1)/2
      energy_mat = torch.zeros([n_nodes, n_nodes, 2])
      rxn_nodes = x.narrow(0, offset, n_nodes)
      
      
      rxn_embedding = torch.mean(rxn_nodes, 0)
      combos = get_combo_indices(n_nodes)

      rxn_pairs = []
      
      for combo in combos:
        rxn_pair = torch.cat((rxn_nodes[combo[0]] + rxn_nodes[combo[1]], rxn_embedding))
        rxn_pairs.append(rxn_pair)

      
      rxn_pairs_t = torch.stack(rxn_pairs, dim = 0)
      all_rxns.append(rxn_pairs_t)
      offset += n_nodes
      #energies = self.energy(rxn_pairs_t)
      #print(energies)

      '''

      for node in range(n_nodes):
        left = node
        right = node # + 1 #don't calculate self-pairing
        for right_node in range(right, n_nodes):
          left_index = left + offset
          right_index = right_node + offset
          
          solvent=solvent_list[solvent_slice : solvent_slice + self.n_solvents]
          solvent = torch.tensor(solvent).float()
          pair_energy = self.energy(torch.cat((x[left_index] + x[right_index], rxn_embedding)))
          
          energy_mat[left][right_node] = pair_energy
          energy_mat[right_node][left]= pair_energy

      offset += n_nodes
      solvent_slice += self.n_solvents
      
      pair = emit_pair(energy_mat)

      if results == None:
        results = pair
      else:
        results = torch.cat((results, pair))
    '''
    all_rxns_t = torch.cat(all_rxns)
    results = self.energy(all_rxns_t)
    return results   

    return(x)
  
def flatten_and_concat_matrices(rxn_mat_list):
  flattened = None
  res = []
  for rxn_mat in rxn_mat_list:
    indices = get_combo_indices(rxn_mat.shape[0])
    
    for index in indices:
      i_x = index[0]
      i_y = index[1]
      res.append(rxn_mat[i_x][i_y])

    '''
    flat_mat = torch.from_numpy(rxn_mat)
    flat_mat = torch.flatten(flat_mat)
    if flattened == None:
      flattened = flat_mat
    else:
      flattened = torch.cat((flattened, flat_mat))
    
    print(flattened.shape)
    '''
  return(torch.tensor(res))



def train(model, train_loader, device, optimizer, epoch):
  model.train()
  loss_all = 0
  '''
  train_loader.to(device)
  optimizer.zero_grad()
  output = model(train_loader)

  loss = F.nll_loss(output, train_loader.y)
  loss.backward()
  loss_all += loss.item() * 1913
  optimizer.step()
  '''

  
  for data in train_loader:
    
    data = data.to(device)

    optimizer.zero_grad()
    output = model(data)
    
    res = flatten_and_concat_matrices(data.rxn_mat)      
    #loss = nn.MSELoss()
    #loss = nn.BCELoss()
    loss = nn.CrossEntropyLoss(weight=torch.tensor([1, 3051/78.0]))
    #loss = nn.CrossEntropyLoss()
    loss = loss(output, res)
    loss.backward()
    loss_all += loss.item() * data.num_graphs
    optimizer.step()
  return loss_all 


def test(loader, model, device):
  model.eval()
  correct = 0
  total = 0
  total_pos = 0
  true_pos = 0
  for data in loader:
    labels = flatten_and_concat_matrices(data.rxn_mat)      
    #print(data)
    #print(data.edge_index)
    data = data.to(device)
    total += len(labels)
    curr_pos = labels != 0
    total_pos += curr_pos.sum().item()

    output = model(data)
    pred = output.max(dim=1)[1]
    correct += pred.eq(labels).sum().item()
    # both prediction and label were positive
    outcomes = torch.logical_and(pred == labels, pred > 0)

      
    true_pos += outcomes.sum().item()


    
  return (correct / total), (true_pos/total_pos)


def net(train_loader, validation_loader, n_solvents):
  n_atom_features=75
  n_edge_features=6
  embed_dim=30
  

  device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


  #model = Atom_Reactivity_Net(n_atom_features, embed_dim, n_edge_features).to(device)
  model = Atom_Pair_Reactivity_Net(n_atom_features, embed_dim, n_edge_features, n_solvents).to(device)

  


  optimizer = torch.optim.Adam(model.parameters(), lr=0.001 * np.sqrt(.125))
  hist = {"loss":[], "acc":[], "test_acc":[], "tpr":[], "test_tpr":[]}
  num_epochs = 101#101
  for epoch in range(1, num_epochs):

    train_loss = train(model, train_loader, device, optimizer, epoch)
    
    
    train_acc, train_tpr = test(train_loader, model, device)
    test_acc, test_tpr = test(validation_loader, model, device)



    hist["loss"].append(train_loss)
    hist["acc"].append(train_acc)
    hist["test_acc"].append(test_acc)

    hist["tpr"].append(train_tpr)
    hist["test_tpr"].append(test_tpr)
    #print(f'Epoch: {epoch}, Train loss: {train_loss:.3}, Train_acc: {train_acc:.3}, Test_acc: {test_acc:.3}')
    print(f'Epoch: {epoch}, Train loss: {train_loss:.3}, Train_acc: {train_acc:.3}, Test_acc: {test_acc:.3}, Train_tpr: {train_tpr}')
    
    
  
  epcs = [x for x in range(1, num_epochs)]
  plt.subplot(1,2,1)
  plt.title('Accuracy and Sensitivity')
  plt.plot(epcs, hist['acc'], label='Train Accuracy', linewidth=2, color ='orange')
  plt.plot(epcs, hist['test_acc'], label='Test Accuracy', linewidth=2, linestyle='dashed', color = 'orange')
  plt.plot(epcs, hist['tpr'], label='Train TPR', linewidth=2, color='cyan')
  plt.plot(epcs, hist['test_tpr'], label='Test TPR', linewidth=2, linestyle='dashed' , color='cyan')
  plt.legend()
  plt.subplot(1,2,2)
  plt.title('Loss')
  plt.plot(epcs, hist['loss'], linewidth=2, color='black')
  plt.savefig('../figures/atom_pair_performance_weighted_loss_100_rxn_embed.png')
  torch.save(model.state_dict(), '../models_metacyc_100_rxn_embed.pkl')
  





def main():
  #infile = pd.read_csv('../data/50_r.rsmi', sep='\t')
  #infile = pd.read_csv('../data/100_r.rsmi', sep='\t')
  #infile = pd.read_csv('../data/2k_r.rsmi', sep='\t')
  #infile = pd.read_csv('../data/meta_cyc_atom_mappings_1k.tsv', sep='\t')
  infile = pd.read_csv('../data/meta_cyc_atom_mappings_100.tsv', sep='\t')
  dataset, ecs = chem_to_graph.file_to_dataset(infile, biochem=True)

  


  n_solvents = 0#dataset[0].solvents.shape[0]
  batch_size = 16
  validation_split = .2
  shuffle_dataset = True
  random_seed= 42

  # Creating data indices for training and validation splits:
  dataset_size = len(dataset)
  indices = list(range(dataset_size))
  split = int(np.floor(validation_split * dataset_size))
  if shuffle_dataset :
    np.random.seed(random_seed)
    np.random.shuffle(indices)
    train_indices, val_indices = indices[split:], indices[:split]
  # Creating PT data samplers and loaders:
  train_sampler = SubsetRandomSampler(train_indices)
  valid_sampler = SubsetRandomSampler(val_indices)

  train_loader = DataLoader(dataset, batch_size=batch_size, 
                                           sampler=train_sampler)
  validation_loader = DataLoader(dataset, batch_size=batch_size,
                                                sampler=valid_sampler)



  net(train_loader, validation_loader, n_solvents)


if __name__ == "__main__":
  main()
    
