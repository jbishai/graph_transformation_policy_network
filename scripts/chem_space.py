#! /Users/jbishai/miniconda3/envs/gptn/bin/python

from __future__ import division
from __future__ import unicode_literals

import sys
import os
from functools import reduce
import random


import multiprocessing
import logging

from torch_geometric.nn import MessagePassing
from torch_geometric.utils import add_self_loops, degree
from torch_geometric.data import Data, DataLoader, Batch

from torch.nn import Linear
from torch.nn import BatchNorm1d

from torch_geometric.nn import GCNConv
from torch_geometric.nn import GATConv
from torch_geometric.nn import ChebConv
from torch_geometric.nn import global_add_pool, global_mean_pool

from torch_geometric.utils import add_self_loops, degree


from torch_scatter import scatter_mean

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data.sampler import SubsetRandomSampler

import matplotlib.pyplot as plt

import pandas as pd
import numpy as np
import operator

import rxn_to_graphs

DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

def get_loaders(dataset):
    batch_size = 1028
    validation_split = .1
    shuffle_dataset = True
    random_seed = 2
    dataset_size = len(dataset)
    indices = list(range(dataset_size))
    split = int(np.floor(validation_split * dataset_size))
    if shuffle_dataset:
        np.random.seed(random_seed)
        np.random.shuffle(indices)
        train_indices, val_indices = indices[split:], indices[:split]
    train_sampler = SubsetRandomSampler(train_indices)
    valid_sampler = SubsetRandomSampler(val_indices)
    train_loader = DataLoader(dataset, batch_size=batch_size,
                              sampler=train_sampler)
    validation_loader = DataLoader(dataset, batch_size=batch_size,
                                   sampler=valid_sampler)
    return train_loader, validation_loader

class Highway(nn.Module):
    def __init__(self, size, num_layers, f):
        super(Highway, self).__init__()
        self.num_layers = num_layers
        self.nonlinear = nn.ModuleList([nn.Linear(size, size) for _ in range(num_layers)])
        self.linear = nn.ModuleList([nn.Linear(size, size) for _ in range(num_layers)])
        self.gate = nn.ModuleList([nn.Linear(size, size) for _ in range(num_layers)])
        self.f = f
    def forward(self, x):
        """
        :param x: tensor with shape of [batch_size, size]
        :return: tensor with shape of [batch_size, size]
        applies σ(x) ⨀ (f(G(x))) + (1 - σ(x)) ⨀ (Q(x)) transformation | G and Q is affine transformation,
        f is non-linear transformation, σ(x) is affine transformation with sigmoid non-linearition
        and ⨀ is element-wise multiplication
        """
        for layer in range(self.num_layers):
            gate = F.sigmoid(self.gate[layer](x))
            nonlinear = self.f(self.nonlinear[layer](x))
            linear = self.linear[layer](x)
            x = gate * nonlinear + (1 - gate) * linear
        return x


class GNN(MessagePassing):
    def __init__(self, in_channels, n_edge_feats, out_channels):
        super(GNN, self).__init__(aggr='mean')
        self.lin = torch.nn.Linear(in_channels, out_channels)
        self.mlp = nn.Sequential(
            nn.Linear(in_channels * 2 + n_edge_feats, out_channels),
            nn.LeakyReLU()
            )
        self.lin_u =nn.Sequential(
            nn.Linear(out_channels * 2, out_channels),
            nn.LeakyReLU()
            )

        self.highway = Highway(size=in_channels + out_channels, num_layers=1, f=nn.Sigmoid())
    def forward(self, x, edge_index, edge_attr):

        
        x = self.lin(x)
        return self.propagate(edge_index, x=x, edge_attr=edge_attr)

    def message(self, x_i, x_j, edge_attr):
        if edge_attr.size()[0] == 0:
            edge_attr = torch.zeros([0,6])
        tmp = torch.cat([x_i, x_j, edge_attr], dim=1)
        return self.mlp(tmp)

    def update(self, aggr_out, x):
        tmp = torch.cat([x, aggr_out], dim=1)
        tmp = self.highway(tmp)
        return self.lin_u(tmp)

class Node_Embed(torch.nn.Module):
    def __init__(self, in_channels, embed_dim, n_edge_feats, n_layers=1):
        super(Node_Embed, self).__init__()
        self.layers = nn.ModuleList()
        self.init_embed = nn.Sequential(
            nn.Linear(in_channels, embed_dim),
            nn.LeakyReLU())
        for i in range(n_layers):
            self.layers.append(GNN(embed_dim, n_edge_feats, embed_dim))


    def forward(self, x, edge_index, edge_attr):
        x = self.init_embed(x)
        for layer in self.layers:
            x = layer(x, edge_index, edge_attr)
        return x

#https://softwareengineering.stackexchange.com/questions/168572/distance-from-point-to-n-dimensional-line
def distance_point_and_line(p, r, one_step):
    pa = one_step - p
    ba = r - p
    t = torch.sum(pa * ba, dim=-1)/torch.sum(ba * ba, dim=-1)

    dist = pa - (t * ba.T).T
    dist = torch.square(dist)
    dist = torch.sum(dist, dim=1)
    dist = torch.sqrt(dist)
    dist = dist + 0.00000000000001
    return dist

def dist_loss(p, r, one_step):
    p_l_dist = distance_point_and_line(p, r, one_step)

    p_r_dist = torch.sum(p - r, dim=1)**2
    p_1st_dist = torch.sum(p - one_step, dim=1)**2
    '''
    print('p_l distance:')
    print(p_l_dist)
    print('prod_react_dist')
    print(p_r_dist)
    print('p_1st_dist')
    print(p_1st_dist)
    print('points')
    print(p, r, one_step)
    print('distances')
    print(p_l_dist)
    print(p_r_dist)
    print(p_1st_dist)
    '''
    loss = p_l_dist - (p_r_dist - p_1st_dist)

    return loss


def batched_cosine_similarity(vec, batch):

    num = torch.mm(vec, batch.t())
    vec_norm = torch.linalg.norm(vec, dim=1)
    batch_norm = torch.linalg.norm(batch, dim=1)
    denom = torch.mul(vec_norm, batch_norm)
    cosine_sim = (num / denom)
    return(cosine_sim)

def batched_contrastive_loss(r, one_step, temp=10):
    numerator = torch.exp(torch.nn.functional.cosine_similarity(r, one_step, dim=-1)/temp)

    denominator = batched_cosine_similarity(r, r)

    denominator = torch.exp(denominator/temp)

    denominator = torch.sum(denominator, dim=1)
    denominator = denominator - torch.exp(torch.ones_like(denominator))
    loss = torch.div(numerator, denominator)
    loss = torch.log(loss)
    return(loss)


# from https://arxiv.org/pdf/2102.10056.pdf
def contrastive_loss(r, one_step, batch, temp=10):
    cosine_similarity = torch.nn.CosineSimilarity(0)
    numerator = torch.exp(cosine_similarity(r, one_step)/temp)





    denominator = torch.zeros(batch.size()[0], device=DEVICE)
    for x in range(len(batch)):
        denominator += torch.exp(cosine_similarity(r, batch[x])/temp)

    denominator -= torch.exp(torch.ones(1, requires_grad = True, device=DEVICE)/temp)


    loss = torch.log(numerator / denominator) * -1
    return loss

def groupby_mean(value:torch.Tensor, labels:torch.LongTensor) -> (torch.Tensor, torch.LongTensor):
    """Group-wise average for (sparse) grouped tensors

    Args:
        value (torch.Tensor): values to average (# samples, latent dimension)
        labels (torch.LongTensor): labels for embedding parameters (# samples,)

    Returns:
        result (torch.Tensor): (# unique labels, latent dimension)
        new_labels (torch.LongTensor): (# unique labels,)

    Examples:
        >>> samples = torch.Tensor([
                             [0.15, 0.15, 0.15],    #-> group / class 1
                             [0.2, 0.2, 0.2],    #-> group / class 3
                             [0.4, 0.4, 0.4],    #-> group / class 3
                             [0.0, 0.0, 0.0]     #-> group / class 0
                      ])
        >>> labels = torch.LongTensor([1, 5, 5, 0])
        >>> result, new_labels = groupby_mean(samples, labels)

        >>> result
        tensor([[0.0000, 0.0000, 0.0000],
            [0.1500, 0.1500, 0.1500],
            [0.3000, 0.3000, 0.3000]])

        >>> new_labels
        tensor([0, 1, 5])
    """
    uniques = labels.unique().tolist()
    labels = labels.tolist()

    key_val = {key: val for key, val in zip(uniques, range(len(uniques)))}
    val_key = {val: key for key, val in zip(uniques, range(len(uniques)))}

    labels = torch.LongTensor(list(map(key_val.get, labels)))
    labels = labels.to(DEVICE)

    labels = labels.view(labels.size(0), 1).expand(-1, value.size(1))

    unique_labels, labels_count = labels.unique(dim=0, return_counts=True)
    result = torch.zeros_like(unique_labels, dtype=torch.float).scatter_add_(0, labels, value)
    result = result / labels_count.float().unsqueeze(1)
    new_labels = torch.LongTensor(list(map(val_key.get, unique_labels[:, 0].tolist())))
    return result, new_labels

def get_graph_embeddings(model, x, edge_index, edge_attr, rxn_ids):
    node_embeddings = model(x, edge_index, edge_attr)

    graph_embeddings, lab = groupby_mean(node_embeddings, rxn_ids)
    return graph_embeddings, lab

def check_dims(t1, t2):
    if t1.size() != t2.size():
        print('bad thing happened')


def all_to_gpu(data, device):
    #Batch(ec=[9], edge_attr_1step=[332, 6], edge_attr_p=[332, 6], edge_attr_r=[332, 6], edge_attr_rand=[414, 6], edge_index_1step=[2, 332], edge_index_p=[2, 332], edge_index_r=[2, 332], edge_index_rand=[2, 414], molecule_id_p=[9], molecule_id_r=[9], rxn_id=[9], x_1step=[161, 75], x_p=[161, 75], x_r=[161, 75], x_rand=[201, 75])
    
    data.x_p = data.x_p.to(device)
    data.edge_index_p = data.edge_index_p.to(device)
    data.edge_attr_p = data.edge_attr_p.to(device)

    data.x_r = data.x_r.to(device)
    data.edge_index_r = data.edge_index_r.to(device)
    data.edge_attr_r = data.edge_attr_r.to(device)

    data.x_1step = data.x_1step.to(device)
    data.edge_index_1step = data.edge_index_1step.to(device)
    data.edge_attr_1step = data.edge_attr_1step.to(device)

    
    rxn_ids = reduce(operator.add, data.rxn_id)
    rxn_ids = torch.tensor(rxn_ids)
    rxn_ids = rxn_ids.to(device)
    data.rxn_id = rxn_ids
    

    #data.molecule_id_p.to(device)
    #data.molecule_id_r.to(device)
    #data.molecule_id_1step.to(device)

    
        

def get_loss(model, loader, device, optimizer, loss_all, trainset=True):
    if trainset:
        model.train()
    else:
        model.eval()
    n = len(loader)
    for data in loader:
        optimizer.zero_grad()

        all_to_gpu(data, device)
 

        ### debugging
        if not (max(data.edge_index_r[0]) < data.x_r.size()[0]).item():
            print('reactants are screwed')
            print('nodes')
            print(data.x_r.size())
            print('Max edge index')
            print(max(data.edge_index_r[0]))
            
        if not (max(data.edge_index_p[0]) < data.x_p.size()[0]).item():
            print('products are screwed')

            print('nodes')
            print(data.x_p.size())
            print('Max edge index')
            print(max(data.edge_index_p[0]))
            continue

        if not (max(data.edge_index_1step[0]) < data.x_1step.size()[0]).item():
            print('one steps are screwed')

            print('nodes')
            print(data.x_1step.size())
            print('Max edge index')
            print(max(data.edge_index_1step[0]))

        rxn_ids = data.rxn_id
        #rxn_ids = reduce(operator.add, rxn_ids)
        #rxn_ids = torch.tensor(rxn_ids)
        r_embed = model(data.x_r, data.edge_index_r, data.edge_attr_r)
        p_embed = model(data.x_p, data.edge_index_p, data.edge_attr_p)
        one_step_embed = model(data.x_1step, data.edge_index_1step, data.edge_attr_1step)


        #print('embedded nodes')
        #print(r_embed, p_embed, one_step_embed)

        r_graph_embed, lab = groupby_mean(r_embed, rxn_ids)
        p_graph_embed, lab = groupby_mean(p_embed, rxn_ids)
        one_step_graph_embed, lab = groupby_mean(one_step_embed, rxn_ids)


        #r_graph_embed = torch.ones_like(r_graph_embed)
        #one_step_graph_embed = torch.ones_like(one_step_graph_embed)

        b_loss = batched_contrastive_loss(r_graph_embed, one_step_graph_embed, temp=1)

        '''
        loss = contrastive_loss(r_graph_embed[0], one_step_graph_embed[0], r_graph_embed, temp=1)
        for i in range(1, r_graph_embed.size()[0]):
            loss += contrastive_loss(r_graph_embed[i], one_step_graph_embed[i], r_graph_embed, temp=1)


        loss = torch.log(loss)
        '''


        loss = torch.mean(b_loss * -1)

        loss_all += loss

        if trainset:
            loss.backward()
            optimizer.step()

    return loss_all / n






def train(model, train_loader, validation_loader, device, optimizer):

    train_loss = 0
    validation_loss = 0
    train_loss = get_loss(model, train_loader, device, optimizer, train_loss, trainset=True)
    validation_loss = get_loss(model, validation_loader, device, optimizer, validation_loss, trainset=False)


    return train_loss, validation_loss


def train_net(train_loader, validation_loader):
    n_atom_feats = 75
    n_edge_feats = 6
    embed_dim = 256

    #device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    device = DEVICE
    #device = torch.device('cpu')
    model = Node_Embed(n_atom_feats, embed_dim, n_edge_feats, n_layers=3)
    model.to(device)

    num_epochs = 101
    optimizer = torch.optim.Adam(model.parameters(), lr=0.0005)
    train_dict = {}
    valid_dict = {}
    for epoch in range(1, num_epochs):
        train_loss, validation_loss = train(model, train_loader, validation_loader, device, optimizer)
        train_loss = train_loss 
        validation_loss = validation_loss 
        train_dict[epoch] = train_loss.detach()
        valid_dict[epoch] = validation_loss.detach()
        print('Epoch: %i: Train loss:%.4f Validation_loss:%.4f' % (epoch, train_loss, validation_loss), flush=True)
        if epoch % 5 == 0:
            PATH='../models/constrastive_loss_epoch_%i.pt' % (epoch)
            torch.save({
                'epoch': epoch,
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': optimizer.state_dict(),
                'train_loss': train_loss,
                'validation_loss': validation_loss
            }, PATH)
        #print(train_loss)
    fig = plt.figure()
    ax = plt.axes()

    train_vals = train_dict.values()
    valid_vals = valid_dict.values()

    train_vals = [x.detach().cpu() for x in train_vals]

    valid_vals = [x.detach().cpu() for x in valid_vals]
    
    ax.plot(train_dict.keys(), train_vals, label='Train Loss')
    ax.plot(valid_dict.keys(), valid_vals, label='Validation Loss' )
    ax.legend()
    plt.savefig('../figures/loss_plot_contrastive_loss.pdf')

    torch.save(model.state_dict(), '../models/contrastive_loss.pt')


def embeddings_to_df(model, loader):
    #device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    device = DEVICE
    model.to(device)
    embed_dim = 256
    all_r = []
    all_p = []
    all_1s = []
    labs = []
    for data in loader:
        data = data.to(device)
        all_to_gpu(data, device)
        r_embed, r_lab = get_graph_embeddings(model, data.x_r, data.edge_index_r, data.edge_attr_r, data.rxn_id)
        p_embed, p_lab = get_graph_embeddings(model, data.x_p, data.edge_index_p, data.edge_attr_p, data.rxn_id)
        one_s_embed, one_s_lab = get_graph_embeddings(model, data.x_1step, data.edge_index_1step, data.edge_attr_1step, data.rxn_id)


        labs.append(r_lab)
        all_r.append(r_embed)
        all_p.append(p_embed)
        all_1s.append(one_s_embed)

    labs = torch.cat(labs).detach().cpu().numpy()
    all_r = torch.cat(all_r).detach().cpu().numpy()
    all_p = torch.cat(all_p).detach().cpu().numpy()
    all_1s = torch.cat(all_1s).detach().cpu().numpy()


    feat_labs = ['feature_' + str(x) for x in range(embed_dim)]
    df_r = pd.DataFrame(all_r, columns=feat_labs)
    df_p = pd.DataFrame(all_p, columns=feat_labs)
    df_1s = pd.DataFrame(all_1s, columns=feat_labs)

    df_r['rxn_id'] = labs
    df_p['rxn_id'] = labs
    df_1s['rxn_id'] = labs
    return df_r, df_p, df_1s

def get_embeddings(loader):
    n_atom_feats = 75
    n_edge_feats = 6
    embed_dim = 256
    model = Node_Embed(n_atom_feats, embed_dim, n_edge_feats, n_layers=3)

    model.eval()

    df_null_r, df_null_p, df_null_1s = embeddings_to_df(model, loader)

    df_null_r.to_csv('../outputs/null_r_embeddings.csv')
    df_null_p.to_csv('../outputs/null_p_embeddings.csv')
    df_null_1s.to_csv('../outputs/null_1s_embeddings.csv')

    model.load_state_dict(torch.load('../models/contrastive_loss.pt'))

    df_r, df_p, df_1s = embeddings_to_df(model, loader)

    df_r.to_csv('../outputs/r_embeddings.csv')
    df_p.to_csv('../outputs/p_embeddings.csv')
    df_1s.to_csv('../outputs/1s_embeddings.csv')




def main():
    random_seed = 42
    torch.manual_seed(random_seed)
    random.seed(random_seed)

    #local side
    #infile = pd.read_csv('../test_data/meta_cyc_atom_mappings_5.tsv', sep='\t')
    #dataset = rxn_to_graphs.file_to_rxns(infile, '../test_data/metacyc_5.pt')

    #server side
    infile = pd.read_csv('../data/meta_cyc_atom_mappings.tsv', sep='\t')
    dataset = rxn_to_graphs.file_to_rxns(
        infile,
        '../data/metacyc_full.pt',
        ['../data/rxn_to_ecs_onehot.csv',
         '../data/first_ecs_onehot.csv',
         '../data/second_ecs.onehot.csv']
    )

    #infile = pd.read_csv('../data/meta_cyc_atom_mappings_25.tsv', sep='\t')

    train_loader, validation_loader = get_loaders(dataset)

    train_net(train_loader, validation_loader)
    get_embeddings(train_loader)



if __name__ == '__main__':
    main()
