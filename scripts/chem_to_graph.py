from __future__ import division
from __future__ import unicode_literals

import sys
import os
from functools import reduce
from collections import defaultdict
import random
import pandas as pd
import pickle

import torch
import rdkit
from rdkit import Chem
from rdkit.Chem import AllChem
import numpy as np
from rdkit.Chem import Draw

from rdkit.Chem import rdFMCS

from torch_geometric.data import Data, DataLoader, Batch

import pythoncyc
from pythoncyc.PToolsFrame import PFrame



# following code was borrowed from deepchem
# https://raw.githubusercontent.com/deepchem/deepchem/master/deepchem/feat/graph_features.py
 
 
def one_of_k_encoding(x, allowable_set):
  if x not in allowable_set:
    raise Exception("input {0} not in allowable set{1}:".format(
        x, allowable_set))
  return list(map(lambda s: x == s, allowable_set))
 
 
def one_of_k_encoding_unk(x, allowable_set):
  """Maps inputs not in the allowable set to the last element."""
  if x not in allowable_set:
    x = allowable_set[-1]
  return list(map(lambda s: x == s, allowable_set))
 
 
def get_intervals(l):
  """For list of lists, gets the cumulative products of the lengths"""
  intervals = len(l) * [0]
  # Initalize with 1
  intervals[0] = 1
  for k in range(1, len(l)):
    intervals[k] = (len(l[k]) + 1) * intervals[k - 1]
 
  return intervals
 
 
def safe_index(l, e):
  """Gets the index of e in l, providing an index of len(l) if not found"""
  try:
    return l.index(e)
  except:
    return len(l)
 
 
possible_atom_list = [
    'C', 'N', 'O', 'S', 'F', 'P', 'Cl', 'Mg', 'Na', 'Br', 'Fe', 'Ca', 'Cu',
    'Mc', 'Pd', 'Pb', 'K', 'I', 'Al', 'Ni', 'Mn'
]
possible_numH_list = [0, 1, 2, 3, 4]
possible_valence_list = [0, 1, 2, 3, 4, 5, 6]
possible_formal_charge_list = [-3, -2, -1, 0, 1, 2, 3]
possible_hybridization_list = [
    Chem.rdchem.HybridizationType.SP, Chem.rdchem.HybridizationType.SP2,
    Chem.rdchem.HybridizationType.SP3, Chem.rdchem.HybridizationType.SP3D,
    Chem.rdchem.HybridizationType.SP3D2
]
possible_number_radical_e_list = [0, 1, 2]
possible_chirality_list = ['R', 'S']
 
reference_lists = [
    possible_atom_list, possible_numH_list, possible_valence_list,
    possible_formal_charge_list, possible_number_radical_e_list,
    possible_hybridization_list, possible_chirality_list
]
 
intervals = get_intervals(reference_lists)
 
 
def get_feature_list(atom):
  features = 6 * [0]
  features[0] = safe_index(possible_atom_list, atom.GetSymbol())
  features[1] = safe_index(possible_numH_list, atom.GetTotalNumHs())
  features[2] = safe_index(possible_valence_list, atom.GetImplicitValence())
  features[3] = safe_index(possible_formal_charge_list, atom.GetFormalCharge())
  features[4] = safe_index(possible_number_radical_e_list,
                           atom.GetNumRadicalElectrons())
  features[5] = safe_index(possible_hybridization_list, atom.GetHybridization())
  return features
 
 
def features_to_id(features, intervals):
  """Convert list of features into index using spacings provided in intervals"""
  id = 0
  for k in range(len(intervals)):
    id += features[k] * intervals[k]
 
  # Allow 0 index to correspond to null molecule 1
  id = id + 1
  return id
 
 
def id_to_features(id, intervals):
  features = 6 * [0]
 
  # Correct for null
  id -= 1
 
  for k in range(0, 6 - 1):
    # print(6-k-1, id)
    features[6 - k - 1] = id // intervals[6 - k - 1]
    id -= features[6 - k - 1] * intervals[6 - k - 1]
  # Correct for last one
  features[0] = id
  return features
 
 
def atom_to_id(atom):
  """Return a unique id corresponding to the atom type"""
  features = get_feature_list(atom)
  return features_to_id(features, intervals)
 
 
def atom_features(atom,
                  bool_id_feat=False,
                  explicit_H=False,
                  use_chirality=False):

  if bool_id_feat:
    return np.array([atom_to_id(atom)])
  else:
    results = one_of_k_encoding_unk(
      atom.GetSymbol(),
      [
        'C',
        'N',
        'O',
        'S',
        'F',
        'Si',
        'P',
        'Cl',
        'Br',
        'Mg',
        'Na',
        'Ca',
        'Fe',
        'As',
        'Al',
        'I',
        'B',
        'V',
        'K',
        'Tl',
        'Yb',
        'Sb',
        'Sn',
        'Ag',
        'Pd',
        'Co',
        'Se',
        'Ti',
        'Zn',
        'H',  # H?
        'Li',
        'Ge',
        'Cu',
        'Au',
        'Ni',
        'Cd',
        'In',
        'Mn',
        'Zr',
        'Cr',
        'Pt',
        'Hg',
        'Pb',
        'Unknown'
      ]) + one_of_k_encoding(atom.GetDegree(),
                             [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]) + \
                             one_of_k_encoding_unk(atom.GetImplicitValence(), [0, 1, 2, 3, 4, 5, 6]) + \
                             [atom.GetFormalCharge(), atom.GetNumRadicalElectrons()] + \
                             one_of_k_encoding_unk(atom.GetHybridization(), [
                               Chem.rdchem.HybridizationType.SP, Chem.rdchem.HybridizationType.SP2,
                               Chem.rdchem.HybridizationType.SP3, Chem.rdchem.HybridizationType.
                               SP3D, Chem.rdchem.HybridizationType.SP3D2
                             ]) + [atom.GetIsAromatic()]
    # In case of explicit hydrogen(QM8, QM9), avoid calling `GetTotalNumHs`
    if not explicit_H:
      results = results + one_of_k_encoding_unk(atom.GetTotalNumHs(),
                                                  [0, 1, 2, 3, 4])
    if use_chirality:
      try:
        results = results + one_of_k_encoding_unk(
          atom.GetProp('_CIPCode'),
          ['R', 'S']) + [atom.HasProp('_ChiralityPossible')]
      except:
        results = results + [False, False
        ] + [atom.HasProp('_ChiralityPossible')]
            
  return np.array(results)
 
 
def bond_features(bond, use_chirality=False):
  from rdkit import Chem
  bt = bond.GetBondType()
  bond_feats = [
      bt == Chem.rdchem.BondType.SINGLE, bt == Chem.rdchem.BondType.DOUBLE,
      bt == Chem.rdchem.BondType.TRIPLE, bt == Chem.rdchem.BondType.AROMATIC,
      bond.GetIsConjugated(),
      bond.IsInRing()
  ]
  if use_chirality:
    bond_feats = bond_feats + one_of_k_encoding_unk(
        str(bond.GetStereo()),
        ["STEREONONE", "STEREOANY", "STEREOZ", "STEREOE"])
  return np.array(bond_feats)
 
#################
# pen added
#################
def get_bond_pair(mol):
  bonds = mol.GetBonds()
  res = [[],[]]
  for bond in bonds:
    res[0] += [bond.GetBeginAtomIdx(), bond.GetEndAtomIdx()]
    res[1] += [bond.GetEndAtomIdx(), bond.GetBeginAtomIdx()]
  return res

def mol2vec(mol, rxn_id, reactant_id, ec):
    mol.UpdatePropertyCache()
    atoms = mol.GetAtoms()
    bonds = mol.GetBonds()
    node_f= [atom_features(atom) for atom in atoms]
    edge_index = get_bond_pair(mol)
    edge_attr = [bond_features(bond, use_chirality=False) for bond in bonds]
    for bond in bonds:
        edge_attr.append(bond_features(bond))
    
    data = Data(x=torch.tensor(node_f, dtype=torch.float),
                edge_index=torch.tensor(edge_index, dtype=torch.long),
                edge_attr=torch.tensor(edge_attr,dtype=torch.float))

    data.__setitem__('rxn_id', rxn_id)
    data.__setitem__('reactant_id', [reactant_id for x in range(len(atoms))])
    data.__setitem__('ec', [ec for x in range(len(atoms))])

    return data


def mol_with_atom_index(mol, reaction_no, react_or_prod_str):
  for atom in mol.GetAtoms():
    atom.SetAtomMapNum(atom.GetIdx())
  fig = Draw.MolToMPL(mol)

  fig.savefig('../figures/' + reaction_no + '/' + react_or_prod_str + '.jpg', bbox_inches='tight')
  return mol

def atom_map_dict(mol):
  # returns a dictionary of format {atom in molecule:unique atom index in reaction}
  ind_to_map = {}
  map_to_ind = {}
  for atom in mol.GetAtoms():
    map_num = atom.GetAtomMapNum()
    if map_num:
      ind_to_map[atom.GetIdx()] = map_num-1
      map_to_ind[map_num-1] = atom.GetIdx()
  return ind_to_map, map_to_ind
  

def find_mappings_of_bonded(bonds, ind_to_map, target_idx):
  bonded = set()
  for bond in bonds:
    start_idx = bond.GetBeginAtomIdx()
    end_idx = bond.GetEndAtomIdx()
    if start_idx == target_idx and end_idx in ind_to_map:
      bonded.add(ind_to_map[end_idx])
    elif end_idx == target_idx and start_idx in ind_to_map:
      bonded.add(ind_to_map[start_idx])
  return(bonded)
    

POSSIBLE_BONDS = {(True, False, False, False, False, False):1, (False, False, True, False, False, False):2, (False, True, False, False, False, False):3, (True, False, False, False, False, True):4, (False, True, False, False, False, True):5}

class ReactingAtom():
  def get_bonded_atoms(self, bonds, target_idx):
  
    bonded = set()
    for bond in bonds:
      bond_key = tuple(bond_features(bond).tolist())
      bond_encoding = POSSIBLE_BONDS[bond_key]

      
      start_idx = bond.GetBeginAtomIdx()
      end_idx = bond.GetEndAtomIdx()
      if start_idx == target_idx:
        bonded.add((end_idx, bond_encoding))
      elif end_idx == target_idx:
        bonded.add((start_idx, bond_encoding))
      
    return bonded
  def __init__(self, graph_ind, atom):
    self.graph_index = graph_ind
    self.index_in_mol = atom.GetIdx()
    self.atom_map = atom.GetAtomMapNum()
    self.bonds_in_molecule = self.get_bonded_atoms(atom.GetBonds(), self.index_in_mol)
    self.bonds_in_graph=[]
  def is_atom_map_match(atom_map):
    return atom_map == self.atom_map
  def is_index_match(self,idx):
    return idx == self.index_in_mol
  def convert_within_mol_bonds_to_graph_bonds(self, molecule):
    new_bonds = []
    for bond in self.bonds_in_molecule:
      new_bonds += [(atom.graph_index, bond[1]) for atom in molecule if atom.is_index_match(bond[0])]
    self.bonds_in_graph = set(new_bonds)
  
    
    
def symmetric_update_reaction_matrix(rxn_matrix, idx1, idx2, bond_encoding):
  #bond_encoding=1
  rxn_matrix[idx1][idx2] = bond_encoding
  rxn_matrix[idx2][idx1] = bond_encoding
  return rxn_matrix
  


def find_reacting_pair(rxn):
  rxn.Initialize()
  rxn.RemoveUnmappedReactantTemplates()
  
  reactants = rxn.GetReactants()
  products = rxn.GetProducts()
  #reacting_atoms = rxn.GetReactingAtoms()
  react_ind_to_maps = []

  all_reactant_atoms = []
  graph_ind = 0
  map_idx_to_graph_idx = {}
  for reactant in reactants:
    molecule = []
    for atom in reactant.GetAtoms():
      r_atom = ReactingAtom(graph_ind, atom)
      molecule.append(r_atom)
      map_idx_to_graph_idx[atom.GetAtomMapNum()] = graph_ind
      all_reactant_atoms.append(r_atom)
      graph_ind += 1
    for atom in molecule:
      atom.convert_within_mol_bonds_to_graph_bonds(molecule)


  all_product_atoms = {}
  for product in products:
    molecule = []
    for atom in product.GetAtoms():
      if atom.GetAtomMapNum() in map_idx_to_graph_idx:
        p_atom = ReactingAtom(map_idx_to_graph_idx[atom.GetAtomMapNum()], atom)
        molecule.append(p_atom)
    for atom in molecule:
      atom.convert_within_mol_bonds_to_graph_bonds(molecule)
      all_product_atoms[atom.graph_index] = atom

  rxn_matrix = np.zeros([len(all_reactant_atoms)] * 2, dtype = int)

  for atom in all_reactant_atoms:
    graph_idx = atom.graph_index
    if graph_idx in all_product_atoms:
      r_bonded = atom.bonds_in_graph

      p_bonded = all_product_atoms[graph_idx].bonds_in_graph
      reacting_atoms = r_bonded.symmetric_difference(p_bonded)


      for r_atom in reacting_atoms:
        symmetric_update_reaction_matrix(rxn_matrix, graph_idx, r_atom[0], r_atom[1])
    else:
      # atom lost from product
      reacting_atoms = atom.bonds_in_graph
      for r_atom in reacting_atoms:
        symmetric_update_reaction_matrix(rxn_matrix, graph_idx, r_atom[0], 6)


  return(rxn_matrix)
      
def combine_graphs(g1, g2):
  #increment every node in the second adjacency list by the number of nodes in the first adjacency list
  index_increment = g1.x.size()[0]
  combined_edge_index = torch.cat((g1.edge_index, g2.edge_index + index_increment), dim = 1)
  combined_edge_attr = torch.cat((g1.edge_attr, g2.edge_attr), dim = 0)
  combined_x = torch.cat((g1.x, g2.x), dim = 0)
  #combined_y = torch.cat((g1.y, g2.y), dim = 0)
  
  
  reactant_ids = g1.reactant_id +  g2.reactant_id
  ecs = g1.ec + g2.ec
  
  combined = Data(x=combined_x,
                  edge_index=combined_edge_index,
                  edge_attr=combined_edge_attr)
  combined.__setitem__('rxn_id', g1.rxn_id)
  combined.__setitem__('reactant_id', reactant_ids)
  combined.__setitem__('ec', ecs)
  return(combined)

def check_equivalency(r, p):
  print(torch.sum(torch.sub(r.edge_attr, p.edge_attr)))
            
    



def encode_reactants(rxn, rxn_id, solvent_dict={}, rxn_to_solvent_dict={}, ec='', reverse=True):
  data = []
  rxn.Initialize()
  agents = rxn.GetAgents()

  rxn.RemoveUnmappedReactantTemplates()

  rxn_matrix = find_reacting_pair(rxn)

  # Metacyc encodes their reactions backwards for some opaque reason
  # https://biocyc.org/PGDBConceptsGuide.shtml#TAG:__tex2page_sec_4.6

  if reverse:
    reactants = rxn.GetProducts()
    products = rxn.GetReactants()

    r = mol2vec(reactants[0], 0, 0, 0)
    p = mol2vec(products[0], 0, 0, 0)
    #check_equivalency(r, p)

  else:
    reactants = rxn.GetReactants()
    products = rxn.GetProducts()

  for agent in agents:
    agent_smiles =  Chem.MolToSmiles(agent)
    if agent_smiles not in solvent_dict:
      solvent_dict[agent_smiles] = len(solvent_dict.keys())
    rxn_to_solvent_dict[rxn_id].append(solvent_dict[agent_smiles])

  reactant_graphs = []

  reactant_id = 0
  for reactant in reactants:
    atoms = reactant.GetAtoms()

    reactant_graph = mol2vec(reactant, rxn_id, reactant_id, ec)
    reactant_id += 1
    reactant_graphs.append(reactant_graph)

  combined = reduce((lambda x,y: combine_graphs(x,y)), reactant_graphs)
  return([combined])
      
def generate_solvent_tensor(n_solvents, rxn_to_solvent_dict, rxn_id):
  solvents = rxn_to_solvent_dict[rxn_id]
  solvent_list = [1 if x in solvents else 0 for x in range(n_solvents)]
  return torch.tensor(solvent_list)

  

def file_to_dataset(infile, biochem=False):
  #infile = pd.read_csv('../data/50_r.rsmi', sep='\t')
  #infile = pd.read_csv('../data/100_r.rsmi', sep='\t')
  #infile = pd.read_csv('../data/2k_r.rsmi', sep='\t')

  reaction_no = 0
  dataset = []
  solvent_dict = {}
  rxn_to_solvent_dict = defaultdict(lambda: [])
  enzyme_map = {}
  rxn_to_ec_map = {}
  enzyme_prot_map = {}
  if biochem:
    meta = pythoncyc.select_organism('meta')
    for rsmile, rxn in zip(infile['ReactionSmiles'], infile['rxn']):
      try:
        pf = PFrame(rxn, meta, getFrameData=True )
        enzymes = []
        for er in pf.enzymatic_reaction:
          erxn = PFrame(er, meta)
          if erxn:
            enzymes.append(erxn.enzyme.replace('|', ''))
        enzyme_prot_map[rsmile] = enzymes
          
        enzyme_map[rsmile] = pf.ec_number
      except:
        pass
    with open('../data/enzyme_mappings.pkl', 'wb') as outF:
      pickle.dump(enzyme_prot_map, outF, protocol=pickle.HIGHEST_PROTOCOL)
    with open('../data/ec_mappings.pkl', 'wb') as outF:
      pickle.dump(enzyme_map, outF)
        
  for rxn in infile['ReactionSmiles']:
    reaction_no += 1
    #os.mkdir('../figures/reaction_' + str(reaction_no))

    if rxn in enzyme_map:
      rxn_to_ec_map[reaction_no] = enzyme_map[rxn]
    
    rxn=rxn.split(' ')[0]
    if reaction_no not in []:
      try:
        r = AllChem.ReactionFromSmarts(rxn)
        if biochem:
          dataset += encode_reactants(r, reaction_no, solvent_dict, rxn_to_solvent_dict, enzyme_map[rxn])
        else:
          dataset += encode_reactants(r, reaction_no, solvent_dict, rxn_to_solvent_dict)
      except:
        
        print(reaction_no)
       

  n_solvents = len(solvent_dict.keys())
  for data in dataset:
    solvents = generate_solvent_tensor(n_solvents, rxn_to_solvent_dict, data.rxn_id)
    data.__setitem__('solvents', solvents)


  return dataset, rxn_to_ec_map


def file_to_react_and_prod(infile, biochem=False):
  reactants = []
  products = []
  reaction_no = 0

  for rxn in infile['ReactionSmiles']:

    reaction_no += 1
    #os.mkdir('../figures/reaction_' + str(reaction_no))

    
    
    rxn=rxn.split(' ')[0]
    if reaction_no not in []:
      
      try:
        r = AllChem.ReactionFromSmarts(rxn)
        
        reactants += encode_reactants(r, reaction_no)
        products += encode_reactants(r, reaction_no, reverse = False)
      except:
        
        print(reaction_no)
  return(reactants, products)
  


if __name__ == '__main__':
  infile = pd.read_csv('../data/meta_cyc_atom_mappings_20.tsv', sep='\t')
  r,p = file_to_react_and_prod(infile, biochem=False)

  print('Number of reactions successfully encoded: %i' % len(r))
